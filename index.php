<?php
include 'libs/load.php';
?>

<!doctype html>
<html lang="en">

<?php load_template('_head') ?>

<body>
  <header>
    <?php load_template('_header'); ?>
  </header>

  <main>

    <?php load_template('_callToAction') ?>

    <?php load_template('_photogram') ?>

  </main>

  <?php load_template('_footer') ?>

  <script src="./assets/dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>