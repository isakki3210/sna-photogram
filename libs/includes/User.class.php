<?php
class User
{
    private $conn;
    private $username;
    private $id;
    public function __call($name, $arguments)
    {
        $property = preg_replace("/[^0-9a-zA-Z]/", "", substr($name, 3));
        $property = strtolower(preg_replace('/\B([A-Z])/', '_$1', $property));
        // print($property);
        if (substr($name, 0, 3) == 'get') {
            return $this->_get_data($property);
        } elseif (substr($name, 0, 3) == 'set') {
            return $this->_set_data($property, $arguments[0]);
        } else {
            throw new Exception("User::__call -> $name , function Unavailable");
        }
    }
    public static function signup($user, $pass, $email, $phone)
    {
        $options = [
            'cost' => 7,
        ];
        $pass = password_hash($pass, PASSWORD_BCRYPT, $options);
        $conn = Database::getConnection();

        $sql = "INSERT INTO `auth` (`username`, `password`, `email`, `phone`, `active` , `blocked`) VALUES ('$user', '$pass', '$email', '$phone', '1' , '0')";

        // $error = false;
        $res = true;
        if ($conn->query($sql) === TRUE) {
            return $res;
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $conn->close();
    }
    public static function login($user, $pass)
    {
        $conn = Database::getConnection();
        $query = "SELECT * FROM `auth` WHERE `username` = '$user'";
        $res = $conn->query($query);
        if ($res->num_rows == 1) {
            $row = $res->fetch_assoc();
            if (password_verify($pass, $row['password'])) {
                return $row['username'];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function __construct($username)
    {
        $this->conn = Database::getConnection();
        $this->username = $username;
        $sql = "SELECT `id` FROM `auth` WHERE `username`= '$username' OR `id` = '$username' LIMIT 1";
        // print($sql);
        $result = $this->conn->query($sql);
        if ($result->num_rows) {
            $row = $result->fetch_assoc();
            $this->id = $row['id'];
        } else throw new Exception("Username does't exist");
    }

    private function _get_data($var)
    {
        // print("hello , $var");
        if (!$this->conn) {
            $this->conn = Database::getConnection();
        }
        $sql = "SELECT `$var` FROM `user` WHERE `id` = $this->id";
        // print($sql);
        $result = $this->conn->query($sql);
        if ($result and $result->num_rows) {
            // print($result->fetch_assoc()["$var"]);
            return $result->fetch_assoc()["$var"];
        } else  return null;
    }

    private function _set_data($var, $data)
    {
        if (!$this->conn) {
            $this->conn = Database::getConnection();
        }
        $sql = "UPDATE `user` SET `$var`='$data' WHERE `id`=$this->id";
        // print($sql);
        if ($this->conn->query($sql)) {
            return true;
        } else return false;
    }

    public function authenticate()
    {
    }

    // public function setBio($bio)
    // {
    //     return $this->_set_data('bio', $bio);
    // }

    // public function getBio()
    // {
    //     return $this->_get_data('bio');
    // }

    // public function setAvatar($link)
    // {
    //     return $this->_set_data('avatar', $link);
    // }

    // public function getAvatar()
    // {
    //     return $this->_get_data('avatar');
    // }

    // public function setFirstname($name)
    // {
    //     return $this->_set_data("first_name", $name);
    // }

    // public function getFirstname()
    // {
    //     return $this->_get_data('first_name');
    // }

    // public function setLastname($name)
    // {
    //     return $this->_set_data("last_name", $name);
    // }

    // public function getLastname()
    // {
    //     return $this->_get_data('last_name');
    // }

    // public function setDob($year, $month, $day)
    // {
    //     if (checkdate($month, $day, $year)) {
    //         return $this->_set_data('dob', "$year.$month.$day");
    //     } else return false;
    // }

    // public function getDob()
    // {
    //     return $this->_get_data('dob');
    // }

    // public function setInstagramlink($link)
    // {
    //     return $this->_set_data('instagram', $link);
    // }

    // public function getInstagramlink()
    // {
    //     return $this->_get_data('instagram');
    // }

    // public function setTwitterlink($link)
    // {
    //     return $this->_set_data('twitter', $link);
    // }

    // public function getTwitterlink()
    // {
    //     return $this->_get_data('twitter');
    // }
    // public function setFacebooklink($link)
    // {
    //     return $this->_set_data('facebook', $link);
    // }

    // public function getFacebooklink()
    // {
    //     return $this->_get_data('facebook');
    // }
}
