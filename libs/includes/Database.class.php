<?php

class Database
{

    public static $conn = null;

    public static function getConnection()
    {
        if (Database::$conn == null) {
            $serverName = get_config('db_server');
            $username = get_config('db_username');
            $password = get_config('db_password');
            $dbname = get_config('db_name');

            $connection = new mysqli($serverName, $username, $password, $dbname);

            if ($connection->connect_error) {
                die("Connection failed: " . $connection->connect_error);
            } else {
                Database::$conn = $connection; //Replacing null with actual connection
                return Database::$conn;
            }
        } else {
            return Database::$conn;
        }
    }
}
