<?php
include_once 'includes/Database.class.php';
include_once 'includes/User.class.php';
include_once 'includes/Session.class.php';
include_once 'includes/UserSession.class.php';

global $__site_config;
$__site_config = file_get_contents('../photogramConfig.json');

Session::start();

function get_config($key)
{
    global $__site_config;
    $array = json_decode($__site_config, true);
    if (isset($array[$key])) {
        return $array[$key];
    } else {
        return null;
    }
}

function load_template($temp_name)
{
    // print("including $temp_name.php");
    include "_templates/$temp_name.php";
}

// function validate_credentials($username , $password){
//     if($username == "access.isakki@gmail.com" and $password == "12345"){
//         return true;
//     }
//     else{
//         return false;
//     }
// }
