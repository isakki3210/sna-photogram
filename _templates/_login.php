<?php
$result = false;
if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $result = User::login($email , $password);
    $result = true;
}
if ($result) {
    echo '<div class="bg-light p-5 rounded mt-3 container">
    <h1>Login Success..!</h1>
    <p class="lead">This example is a quick exercise to do basic login with html forms.</p>
    <!-- <a class="btn btn-lg btn-primary" href="" role="button">View navbar docs »</a> -->
</div>';
} else {
?>
    <style>
        .bg-login-image {
            background: url("./img/nathan-dumlao-lvWw_G8tKsk-unsplash.jpg");
            background-position: center;
            background-size: cover;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-white-900 mb-4">Welcome back! <br><i class="bi bi-bag-check"></i> ShopNew</h1>
                                    </div>
                                    <form action="login.php" method="post">
                                        <div class="mb-3">
                                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                                            <input type="email" placeholder="Enter Email..." class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" autocomplete="off">
                                            <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="exampleInputPassword1" class="form-label">Password</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Enter Password...">
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-3" name="login">LOGIN</button>
                                        <!-- <p class="text-center">New User <a href="register.php">Register Now</a></p> -->
                                        <hr>

                                        <div class="text-center">
                                            <a class="small" href="forget-password.php">Forget Password..</a>
                                        </div>
                                        <div class="text-center">
                                            <a class="small" href="register.php">Create an Account!</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>