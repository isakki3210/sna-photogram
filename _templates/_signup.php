<?php
include_once 'libs/load.php';

$signup = false;
if (isset($_POST['username']) and isset($_POST['password']) and !empty($_POST['username']) and !empty($_POST['password']) and !empty($_POST['phone']) and !empty($_POST['email'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    User::signup($username, $password, $email, $phone);
    $signup = true;
}
if ($signup) {
    echo '<div class="bg-light p-5 rounded mt-3 container">
    <h1>Signup Success..!</h1>
    <p class="lead">This example is a quick exercise to do basic login with html forms.</p>
    <!-- <a class="btn btn-lg btn-primary" href="" role="button">View navbar docs »</a> -->
</div>';
} else {
?>

    <div class="container">
        <style>
            .bg-register-image {
                background: url("./img/brooke-lark-W1B2LpQOBxA-unsplash.jpg");
                background-position: center;
                background-size: cover;
            }
        </style>
        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-register-image"></div>

                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-white-900"><i class="bi bi-bag-check"></i> ShopNew</h1>
                                        <h1 class="h4 text-white-900 mb-3">Register Now!</h1>
                                    </div>
                                    <form action="signup.php" method="post">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">@</span>
                                            <input type="text" class="form-control" placeholder="Username" aria-label="Username" name="username" aria-describedby="basic-addon1">
                                        </div>

                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Email" name="email" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                            <span class="input-group-text" id="basic-addon2">@example.com</span>
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Password</span>
                                            <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">@</span>
                                            <input type="text" class="form-control" placeholder="Phone" aria-label="Username" name="phone" aria-describedby="basic-addon1">
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-3 text-center" name="register">REGISTER</button>
                                        <p class="text-center">
                                            <!-- If you already have an Account <a href="login.php">Login</a> -->
                                        </p>
                                    </form>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
<?php
} ?>