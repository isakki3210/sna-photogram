<?php
include 'libs/load.php';
?>

<!doctype html>
<html lang="en">
<?php load_template('_head'); ?>
<body>
    <header>
        <? load_template('_header'); ?>
    </header>
    <main>
        <?php load_template('_signup') ?>
    </main>
    <!-- <? load_template('_footer') ?> -->
    <script src="./assets/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>